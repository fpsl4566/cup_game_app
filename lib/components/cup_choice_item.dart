 import 'package:flutter/material.dart';

  class CupChoiceItem extends StatefulWidget {
    const CupChoiceItem({
      super.key,
      required this. pandon,
      required this.currentMoney

    });

    final num pandon;//100
    final num currentMoney;//0
    

    @override
    State<CupChoiceItem> createState() => _CupChoiceItemState();
  }

  class _CupChoiceItemState extends State<CupChoiceItem> {
    bool _isOpen = false;
    String imgSrc = '';

    void _calculateImgSrc(){

      //상자깡했을 때 .....(깠을 때)
      //판돈이 현재 돈보다 적을 때 :no.png
      //판돈이 현재 돈과 같을 때 : 100.jpeg
      //판돈이 현재 돈보다 많을 때 : 200.jpeg

      // 안깠을때 .... : 무조건 cup_default.jpeg

      String tempImgSrc = '';
      if(_isOpen){
        if (widget.pandon < widget.currentMoney){
          tempImgSrc = 'no.jpeg';
        } else{
          tempImgSrc = 'money3.png';
        }
      }else {
        tempImgSrc = '100.png';
      }
    }

    @override
    Widget build(BuildContext context) {
      return GestureDetector(
        onTap: (){},
        child: Image.asset(name),
      );
    }
  }
