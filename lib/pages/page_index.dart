import 'package:flutter/material.dart';
class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false;//시작했냐?
    List<num> result = [0,100,200];//[200,0,100]

    void _startGame(){
      setState(() {
        _isStart = true;
      });
    }

  @override
  void initState(){
      super.initState();
      setState(() {

      });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('야바위'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Image.asset('assets/losing.png'),
                Image.asset('assets/losing.png'),
                Image.asset('assets/losing.png'),

              ],
            ),
            Container(
              child: OutlinedButton(
                onPressed: (){},
                child: const Text('다시'),
              ),
            ),
          ],
        ),
      );
    } else {
      return SingleChildScrollView(

        child: Center(
          child: OutlinedButton(onPressed: () {
            _startGame();
          },
            child: const Text('시작'),
          ),
        ),
      );


    }
  }
}
